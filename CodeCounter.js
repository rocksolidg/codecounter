//The code below should not be edited!
const man = `
/**
 * Description of the project:
 * 
 * This program is able to count the rows of your project. If your project is no longer so small that you want to count your rows manually than this little program is a good solution for you.
 * It is very easy to use. 
 * 
 * 
 * Configuration:
 * 
 * In the same directory like this file should be a file called CodeCounterConfig.json. In this file you determine which files are taken into account when counting.
 * 
 * In CodeCounterConfig.json you can specify five constants:
 * 
 * self_written_files (Array):
 * Default: []
 * Description: Single files which are written by you and should be considered.
 * 
 * self_written_directories (Array):
 * Default: ["."]
 * Description: Whole directories which are completly written by you and should be considered.
 * 
 * ignored_files (Array):
 * Default: ['./CodeCounter.js', './CodeCounterConfig.json']
 * Description: Single files which are not written by you and should not be considered but are in a directory which you specified in self_written_directories.
 * 
 * ignored_directories (Array):
 * Default: []
 * Description: Whole directories which are not written by you and should not be considered but are in a directory which you specified in self_written_directories.
 * 
 * ignored_file_types (Array):
 * Default: ["txt"]
 * Description: All files with such a file type will not be condidered.
 * 
 */
`;

//The following source code of the CodeCounter is written by a German. So the names of varibles and functions and the comments are on german.
//Der folgende Quelltext von CodeCounter ist von einem Deutschen geschrieben worden. Folglich sind Namen von Variablen und Funktionen, sowie Kommentare auf Deutsch.

const fs = require('fs');
const path = require('path');

//Globale Variablen:
let selberGeschriebeneDateien = [];
let selberGeschriebeneOrdner = ['.'];
let ausgeschlosseneDateien = ['./CodeCounter.js', './CodeCounterConfig.json'];
let ausgeschlosseneOrdner = [];
let ausgeschlosseneDateiEndungen = ['txt']

//Alle args sind zu Beginn auf false (die Flags also nicht gesetzt)
var args = [
    {
        cmdParamName: '-v',
        value: false,
        descrpition: 'verbose (show many details)'
    },
    {
        cmdParamName: '-f',
        value: false,
        descrpition: 'show considered files'
    },
    {
        cmdParamName: '-c',
        value: false,
        descrpition: 'show initial constants'
    },
    {
        cmdParamName: '--help',
        value: false,
        descrpition: 'show help'
    },
    {
        cmdParamName: '-t',
        value: false,
        descrpition: 'show calculation time'
    },
    {
        cmdParamName: '--man',
        value: false,
        descrpition: 'show manual'
    }
]

main();


async function main() {

    checkArgs();

    if (args[3].value) {
        printHelp();
        return;
    }

    if (args[5].value) {
        printMan();
        return;
    }

    checkConfig();

    if (args[0].value || args[2].value) printConstants();

    const date = new Date();
    const timeStart = date.getTime();

    let alleDateien = await getAlleVonMirGeschriebenenDateienAusserAusgeschlossene();

    if (args[0].value) console.log("Length of each file:\n");
    const gesamteZeilen = getZeilenanzahlVonDateiArray(alleDateien);

    const date2 = new Date();
    const timeEnd = date2.getTime();

    if (args[0].value || args[4].value) console.log("\nCalculation time: " + (timeEnd - timeStart) + " ms\n");

    console.log("Sum of lines of all considered files: " + gesamteZeilen + "");
}

function checkConfig() {
    try {
        const data = fs.readFileSync('./CodeCounterConfig.json', 'utf8');
        const jsonConfigData = JSON.parse(data);

        if (istArray(jsonConfigData.self_written_files)) selberGeschriebeneDateien = jsonConfigData.self_written_files;
        if (istArray(jsonConfigData.self_written_directories)) selberGeschriebeneOrdner = jsonConfigData.self_written_directories;
        if (istArray(jsonConfigData.ignored_files)) ausgeschlosseneDateien = jsonConfigData.ignored_files;
        if (istArray(jsonConfigData.ignored_directories)) ausgeschlosseneOrdner = jsonConfigData.ignored_directories;
        if (istArray(jsonConfigData.ignored_file_types)) ausgeschlosseneDateiEndungen = jsonConfigData.ignored_file_types;

    }
    catch (e) {
        console.log("Can not read or parse the config file ./CodeCounterConfig.json");
        console.log("It will be used the standard configuration instead");
        args[2].value = true;
    }
}

function istArray(moeglichesArray) {
    return Array.isArray(moeglichesArray);
}

function checkArgs() {

    for (let i = 0; i < process.argv.length; i++) {
        const arg = process.argv[i];

        for (let j = 0; j < args.length; j++) {
            if (arg == args[j].cmdParamName) args[j].value = true;
        }
    }

}

// Gibt die Konstanten aus, die der Nutzer am Anfang der Datei festgelegt hat
function printConstants() {
    if (selberGeschriebeneDateien.length != 0) {
        console.log("Manually choosen files: ");
        console.log(selberGeschriebeneDateien);
    }
    if (selberGeschriebeneOrdner.length != 0) {
        console.log("Manually choosen folders: ");
        console.log(selberGeschriebeneOrdner);
    }
    if (ausgeschlosseneDateien.length != 0) {
        console.log("Manually skipped files: ");
        console.log(ausgeschlosseneDateien);
    }
    if (ausgeschlosseneOrdner.length != 0) {
        console.log("Manually skipped folders: ");
        console.log(ausgeschlosseneOrdner);
    }
    if (ausgeschlosseneDateiEndungen.length != 0) {
        console.log("Manually skipped file type");
        console.log(ausgeschlosseneDateiEndungen);
    }
}

function printHelp() {
    let help = `
Usage: node CodeCounter.js [options]

Options:

`;
    for (let i = 0; i < args.length; i++) {
        let tabs = '\t';
        if (args[i].cmdParamName.length <= 5) tabs = tabs.concat('\t');
        const option = '  ' + args[i].cmdParamName + tabs + args[i].descrpition + '\n';
        help = help.concat(option);
    }

    console.log(help);
}

function printMan() {
    console.log(man);
}

// Gibt alle Dateien zurück, welche durch die vier Konstenten (selberGeschriebeneDateien, selberGeschriebeneOrdner, ausgeschlosseneDateien, ausgeschlosseneOrdner) am Anfang festgelegt werden
async function getAlleVonMirGeschriebenenDateienAusserAusgeschlossene() {
    let alleDateien = await getAlleVonMirGeschriebenenDateien();

    for (let i = 0; i < ausgeschlosseneOrdner.length; i++) {
        const dateienDesOrdners = await getAlleDateien(ausgeschlosseneOrdner[i]);
        ausgeschlosseneDateien = ausgeschlosseneDateien.concat(dateienDesOrdners);
    }

    alleDateien = alleDateien.filter(e => !ausgeschlosseneDateien.includes(e));
    alleDateien = alleDateien.filter(e => !ausgeschlosseneDateiEndungen.includes(getEndung(e)));

    if (args[0].value || args[1].value) {
        console.log("\nNumber of finally considered files: " + alleDateien.length);
        console.log("Finally considered files:");
        console.log(alleDateien);
        console.log("Differend file types:");
        console.log(getUnterschiedlicheEndungen(alleDateien));
        console.log('\n');
    }

    return alleDateien;
}

// Gibt alle Dateien zurück, welche durch die zwei Konstenten (selberGeschriebeneDateien, selberGeschriebeneOrdner) am Anfang festgelegt werden
async function getAlleVonMirGeschriebenenDateien() {
    let alleVonMirGeschriebenenDateien = selberGeschriebeneDateien;

    for (let i = 0; i < selberGeschriebeneOrdner.length; i++) {
        const selberGeschriebeneDateienAusOrdner = await getAlleDateien(selberGeschriebeneOrdner[i]);
        alleVonMirGeschriebenenDateien = alleVonMirGeschriebenenDateien.concat(selberGeschriebeneDateienAusOrdner);
    }

    return alleVonMirGeschriebenenDateien;
}

//gibt ein Array zurück, in dem alle relativen Dateipfade der Dateien im Ordner ordnerPath und dessen Unterordnern enthalten sind
async function getAlleDateien(ordnerPath) {
    let dateienVonAktuellemOrdner = [];
    let subordnerVomAltuellemOrdner = [];

    try {
        const dir = await fs.promises.opendir(ordnerPath);
        for await (const dirent of dir) {
            if (dirent.isDirectory()) {
                subordnerVomAltuellemOrdner.push(ordnerPath + '/' + dirent.name);
            }
            else if (dirent.isFile()) {
                dateienVonAktuellemOrdner.push(ordnerPath + '/' + dirent.name);
            }
        }
    }
    catch (e) {
        console.error("Can not open folder: " + ordnerPath);
    }

    let alle = dateienVonAktuellemOrdner;

    for (let i = 0; i < subordnerVomAltuellemOrdner.length; i++) {
        const subordner = subordnerVomAltuellemOrdner[i];
        let weitere = await getAlleDateien(subordner);
        alle = alle.concat(weitere);
    }

    return alle;

}

//Gibt alle unterschiedlichen Endungen in einem zurück (mehrfach vorkommende Endungen werden nur einmal eingefügt)
function getUnterschiedlicheEndungen(pathArray) {
    let unterschiedlicheEndungen = [];

    for (let i = 0; i < pathArray.length; i++) {
        const endung = getEndung(pathArray[i]);
        if (!unterschiedlicheEndungen.includes(endung)) {
            unterschiedlicheEndungen.push(endung);
        }
    }

    return unterschiedlicheEndungen;
}

// Gibt die Dateiendung des Parameter filepath zurück
function getEndung(filepath) {
    let endung = path.extname(filepath);
    let endungOhnePunkt = endung.substring(1, endung.length);
    return endungOhnePunkt;
}

// Gibt die Summe der Zeiolenanzahlen aller Dateinen des Arrays pathArray zurück
function getZeilenanzahlVonDateiArray(pathArray) {
    let zeilen = 0;

    for (let i = 0; i < pathArray.length; i++) {
        const datei = pathArray[i];
        const laenge = getZeilenanzahlVonDatei(datei);
        if (args[0].value) console.log(datei + " : " + laenge);
        zeilen += laenge;
    }

    return zeilen;
}

// Gibt die Zeilenanzahl der Datei filepath zurück
function getZeilenanzahlVonDatei(filepath) {
    try {
        const data = fs.readFileSync(filepath, 'utf8');
        const zeilen = data.split('\n');
        const anzahltZeilen = zeilen.length;
        return anzahltZeilen;
    }
    catch (e) {
        console.error("Can not open file: " + filepath);
        return 0;
    }
}