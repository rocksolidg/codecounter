# Description of the project

This program is able to count the rows of your code. It is a good solution for you if your project is so large that you do not want to count your rows manually.
It is very easy to use.

# Dependencies
If you want to run the source code directly:
- Node.js

# Configuration

A file called CodeCounterConfig.json should be located in the directory which contains the executable file. You determine in this file which files are taken into account when counting.

In CodeCounterConfig.json you can specify five constants:

| Constant | Default value | Description |
| ------------------ | ------------------ | ------------------ |
| *self_written_files* (Array) | [] | Single files which are considered |
| *self_written_directories* (Array) | ["."] | Whole directories which are completly considered including all subdirectories |
| *ignored_files* (Array) | ["./CodeCounter.js", "./CodeCounterConfig.json"] | Single files which should not be considered but are included in a directory which you specified in *self_written_directories* |
| *ignored_directories* (Array) | [] | Whole directories which should not be considered but are included in a directory which you specified in *self_written_directories* |
| *ignored_file_types* (Array) | ["txt"] | All files with such a file extention will not be condidered |

# Usage
## For all operating systems:
If you want to run the source code directly:
```node CodeCounter.js [options]```

## For Windows:
If you want to run the executible file for Windows:
```CodeCounter-win.exe [options]```

## For Linux:
If you want to run the executible file for Linux:
```./CodeCounter-linux [options]```

## For macOS:
If you want to run the executible file for macOS (not tested):
```./CodeCounter-macos [options]```

### Options:

| Option | Description |
| ------------------ | ------------------ |
| --help | show help |
| --man | show manual |
| -v | verbose (show many details)
| -f | show considered files |
| -c | show initial constants |
| -t | show calculation time |
